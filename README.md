Este repositorio contiene el código fuente del libro

    "La resolución de problemas en secundaria"

obra póstuma de Clara Inés Arévalo Merchán. El libro, lamentablemente, no quedó terminado, y estamos trabajando para terminarlo. Escríbenos si te interesa colaborar.

 - No es necesario saber manejar `git`, puedes escribir tus cambio y enviarlos por email (pero por favor intenta usar una versión actualizada y enviarnos tus cambios poco después, para que el texto base no haya cambiado mucho desde que empezaste).
 - No es imprescindible saber `LaTeX`, aunque es conveniente. Puedes registrarte en esta web (desgraciadamente los usuarios pueden tardar un par de días en quedar activos), y rellenar un _"issue"_ si propones un cambio pero no sabes hacerlo.

Salud
